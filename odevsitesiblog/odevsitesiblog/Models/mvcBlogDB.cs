namespace odevsitesiblog.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class mvcBlogDB : DbContext
    {
        public mvcBlogDB()
            : base("name=mvcBlogDB")
        {
        }

        public virtual DbSet<Etiket> Etiket { get; set; }
        public virtual DbSet<Kategori> Kategori { get; set; }
        public virtual DbSet<Makale> Makale { get; set; }
        public virtual DbSet<Uye> Uye { get; set; }
        public virtual DbSet<Yetki> Yetki { get; set; }
        public virtual DbSet<Yorum> Yorum { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Etiket>()
                .HasMany(e => e.Makale)
                .WithMany(e => e.Etiket)
                .Map(m => m.ToTable("MakaleEtiket").MapLeftKey("Etiketid").MapRightKey("Makaleid"));

            modelBuilder.Entity<Yetki>()
                .HasMany(e => e.Uye)
                .WithRequired(e => e.Yetki)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Yorum>()
                .Property(e => e.Tarih)
                .IsFixedLength();
        }
    }
}
