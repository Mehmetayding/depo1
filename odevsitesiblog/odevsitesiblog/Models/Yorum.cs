namespace odevsitesiblog.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Yorum")]
    public partial class Yorum
    {
        public int Yorumid { get; set; }

        [StringLength(50)]
        public string Icerik { get; set; }

        public int? Uyeid { get; set; }

        public int? Makaleid { get; set; }

        [StringLength(10)]
        public string Tarih { get; set; }

        public virtual Makale Makale { get; set; }
    }
}
